/*

goals:

- implement for both vector and array
- methods:
    - all this is in ascending order
    - isSorted (bool)
    - rsort (quicksort with random pivot)
    - msort (quicksort with median pivot)
    - nsort (quicksort with ninther pivot)
    - lsort (quicksort with Lomuto's pivot (last element))
    - hsort (quicksort with Hoare's pivot)
    - shuffle

*/

#include <iostream>
#include <numeric> //for iota
#include <vector>
using std::cout;
using std::vector;

// utility function: print vector

void simpleSwap(vector<int> &v, int start, int end) {
  cout << "Simple swap called for range " << start << " to " << end << "\n";
  if (v[start] > v[end]) {
    cout << "Swapped: " << v[start] << " and " << v[end] << "\n";
    std::swap(v[start], v[end]);
  } else {
    return;
  }
}

void vPrint(vector<int> v) {
  for (auto t : v) {
    cout << t << ", ";
  }
  cout << "\n";
}

int partition(vector<int> &v, int start, int end) {

  // Debug: show what the vector looks like
  cout << "Considering subvector: ";
  for (int i = start; i <= end; i++) {
    cout << v[i] << ", ";
  }

  // Find the pivot value as the median
  cout << "\n";
  int pivotIndex = (start + end) / 2;
  int pivot = v[pivotIndex];

  cout << "Pivot value: " << pivot << "\n";
  // perform v.len() operations
  // if an element is less than the pivot, increment a counting variable
  // now we know how many elements are less than the pivot
  int count = 0;
  for (int i = start; i <= end; i++) {
    if (v[i] <= pivot)
      count++;
  }

  cout << "There are " << count << " elements less than the pivot\n";
  // move the pivot to the correct spot
  // we need to add start and count in case start != 0

  int newPivotIndex = (start + count) - 1;
  // = (start == 0) ? (start + count) - 1 : start + count;

  // swap the pivot with the new pivot index

  if (newPivotIndex != pivotIndex) {
    std::swap(v[pivotIndex], v[newPivotIndex]);

    cout << "Pivot moved: swapped " << v[pivotIndex] << " with "
         << v[newPivotIndex] << "\n";
  } else {
    cout << "Pivot correctly placed\n";
  }

  // now correctly order the elements:
  // this algorithm works pretty well.  we loop through the numbers before the
  // pivot, looking for ones that are too large. if they are, we look for small
  // numbers after the pivot finally, we swap them. not yet tested

  for (int i = start; i <= newPivotIndex; i++) {
    if (v[i] > pivot) {
      // find a number less than the pivot that lies after the pivot
      for (int j = newPivotIndex; j <= end; j++) {
        if (v[j] < pivot) {
          cout << "Swapped " << v[i] << " and " << v[j] << "\n";
          std::swap(v[i], v[j]);
          vPrint(v);
          break;
        }
      }
    }
  }

  cout << "Swap complete: ";
  vPrint(v);
  cout << "Final pivot index: " << newPivotIndex << "\n";
  // this return is important.  In case we select the pivot as being the lowest
  // value in the subvector, we need to continue sorting the rest of the vector.
  // By returning this value, we can ensure the quicksort function will consider
  // the remaining unsorted values.
  return newPivotIndex;
}

// quicksort with median pivot

void quicksort(vector<int> &v, int start, int end) {
  // if the bounds are too small, return
  if (end - start == 1) {
    simpleSwap(v, start, end);
  } else if (end - start < 1) {
    return;
  } else {

    // find the median pivot
    int pivot = partition(v, start, end);
    // partition(v, start, end);
    quicksort(v, start, pivot - 1);
    quicksort(v, pivot + 1, end);
  }
};

// This function is not exactly a Fisher-Yates shuffle.  In the true algorithm,
// randomIndex must be between i and v.size().  In this case, randomIndex is
// between 0 and v.size() Furthermore, this algorithm does not test for
// randomIndex being equal to i, creating the possibility of the vector not
// being sorted at all.
// This probability is 10^10, or 1 in 10 billion, so should not be particularly
// concerning. Statistically, out of 30 billion calls of this function, it will
// fail once.
void fisherYatesShuffle(vector<int> &v) {
  std::srand(time(0));
  // for each element in the array, swap it with a random element
  for (int i = 0; i < v.size(); i++) {
    int randomIndex = rand() % v.size();
    std::swap(v[i], v[randomIndex]);
  }
}

int main() {

  vector<int> v = {10, 7, 1, 5, 4, 6, 11, 3, 2, 8, 9};
  vector<int> subV = {2, 3, 5, 4};

  // quicksort(v, 0, v.size() - 1);

  vector<int> test(10);
  std::iota(test.begin(), test.end(), 1);

  cout << "Original vector: ";
  vPrint(test);

  fisherYatesShuffle(test);

  cout << "Shuffled vector: ";
  vPrint(test);

  quicksort(test, 0, test.size() - 1);

  cout << "Sorted vector: ";
  vPrint(test);
}
